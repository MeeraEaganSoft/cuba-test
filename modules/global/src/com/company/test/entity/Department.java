package com.company.test.entity;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.validation.constraints.NotNull;
import com.haulmont.cuba.core.entity.StandardEntity;
import com.haulmont.chile.core.annotations.NamePattern;

@NamePattern("%s|name")
@Table(name = "TEST_DEPARTMENT")
@Entity(name = "test$Department")
public class Department extends StandardEntity {
    private static final long serialVersionUID = 4771201249286862295L;

    @NotNull
    @Column(name = "NAME", nullable = false, unique = true)
    protected String name;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }


}