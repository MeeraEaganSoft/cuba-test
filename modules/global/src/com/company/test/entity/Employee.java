package com.company.test.entity;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.validation.constraints.NotNull;
import com.haulmont.cuba.core.entity.StandardEntity;
import com.haulmont.chile.core.annotations.NamePattern;

@NamePattern("%s|name")
@Table(name = "TEST_EMPLOYEE")
@Entity(name = "test$Employee")
public class Employee extends StandardEntity {
    private static final long serialVersionUID = 6937236845891620990L;

    @NotNull
    @Column(name = "NAME", nullable = false, length = 50)
    protected String name;

    @NotNull
    @Column(name = "EMAIL", nullable = false, unique = true, length = 100)
    protected String email;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }


}