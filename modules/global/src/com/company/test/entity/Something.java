package com.company.test.entity;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Column;
import com.haulmont.cuba.core.entity.StandardEntity;

@Table(name = "TEST_SOMETHING")
@Entity(name = "test$Something")
public class Something extends StandardEntity {
    private static final long serialVersionUID = 476124145363737888L;

    @Column(name = "IDC")
    protected Boolean idc;

    public void setIdc(Boolean idc) {
        this.idc = idc;
    }

    public Boolean getIdc() {
        return idc;
    }


}